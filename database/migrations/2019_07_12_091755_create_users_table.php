<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('phone')->unique();
            $table->integer('city_id')->unsigned()->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('password');
            $table->string('image')->nullable();
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->string('user_type')->nullable()->default(1);
            $table->integer('points')->nullable()->default(100);
            /*
             * 1 => users
             * 2 => provider ( have a car )
             * 3 => admin have dashboard
             * **/

            $table->integer('main_category_id')->unsigned()->nullable();  // car category id
            $table->smallInteger('verified')->nullable()->default(0); // not active yet
            $table->string('fcm_token')->nullable();
            $table->rememberToken();
            $table->timestamps();


            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('main_category_id')->references('id')->on('main_categories')->onDelete('cascade')->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
