<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Click extends Model
{
    //

    protected $fillable = ['user_id','mobile_click','whatsapp_click'];

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
