<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //

    protected $fillable = [
        'url',
        'terms',
        'app_name',
        'app_token',
        'contact_number',
        'contact_email'
    ];


}
