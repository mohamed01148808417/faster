<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'city_id',
        'email',
        'password',
        'image',
        'lat',
        'long',
        'user_type',
        'main_category_id',
        'verified',
        'fcm_token',
        'points',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }


    public function cars(){
        return $this->hasMany('App\Car','user_id','id');
    }
    public function clicks(){
        return $this->hasMany('App\Click','user_id','id');
    }

    public function pays(){
        return $this->hasMany('App\Pay','user_id','id');
    }


    public function main_category(){
        return $this->belongsTo('App\MainCategory','main_category_id','id');
    }


    public function city(){
        return $this->belongsTo('App\City','city_id','id');
    }

    public function senderChats(){
        return $this->hasMany('App\Chat','sender','id');
    }

    public function receiverChats(){
        return $this->hasMany('App\Chat','receiver','id');
    }

}