<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    protected $fillable = ['title'];

    public function users(){
        return $this->hasMany('App\User','city_id','id');
    }

}
