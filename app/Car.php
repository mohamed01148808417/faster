<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    //

    protected $fillable = [
        'user_id',
        'category_id',
        'main_category_id',
        'car_number',
        'national_image',
        'licence_image',
        'st_image',
        'copy_image',
        'front_image',
        'back_image',
        'lat',
        'long',
    ];


    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function category(){
        return $this->belongsTo('App\Category','category_id','id');
    }

    public function main_category(){
        return $this->belongsTo('App\MainCategory','main_category_id','id');
    }


}
