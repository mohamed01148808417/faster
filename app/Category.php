<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //

    protected $fillable = ['id','title'];

    public function users(){
        return $this->hasMany('App\User','category_id','id');
    }

    public function cars(){
        return $this->hasMany('App\Car','category_id','id');
    }
}
