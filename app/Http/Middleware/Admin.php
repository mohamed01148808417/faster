<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)

    {

        if(\Auth::check()){
            if(auth()->user()->user_type == 3){

                return $next($request);

            }else{
                return redirect('show_login')->with('error','You have not admin access');
            }
        }else{
            return redirect()->route('show_login')->with('error','You have not admin access');
        }


    }

}
