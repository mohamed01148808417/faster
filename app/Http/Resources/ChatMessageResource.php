<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ChatMessageResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'users'=>$this->collection->transform(function ($q){
                return [
                    'id'            =>$q->id,
                    'sender'          =>$q->senderChat->name,
                    'sender_id'          =>$q->senderChat->id,
                    'sender_image'         =>getImg($q->senderChat->image),
                    'receiver'         =>$q->receiverChat->name,
                    'receiver_id'         =>$q->receiverChat->id,
                    'receiver_image'         =>getImg($q->receiverChat->image),
                    'message'         =>$q->message,
                    'attachment'         =>getImg($q->has_file),

                ];
            }),
            'paginate'=>[
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'next_page_url'=>$this->nextPageUrl(),
                'prev_page_url'=>$this->previousPageUrl(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ]
        ];
        //return parent::toArray($request);

    }
}
