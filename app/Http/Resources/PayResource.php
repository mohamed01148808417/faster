<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PayResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'pay'=>$this->collection->transform(function ($q){
                return [
                    'id'            =>$q->id,
                    'user_id'            =>$q->user_id,
                    'user_name'            =>$q->user->name,
                    'pay_image'            =>getImg($q->image),
                    'created_at'            =>$q->created_at
                ];
            }),
            'paginate'=>[
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'next_page_url'=>$this->nextPageUrl(),
                'prev_page_url'=>$this->previousPageUrl(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ]
          ];
        //return parent::toArray($request);
    }
}
