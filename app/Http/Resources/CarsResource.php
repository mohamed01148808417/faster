<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CarsResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'cars'=>$this->collection->transform(function ($q){
                return [
                    'id'                =>$q->id,
                    'user_id'           =>$q->user_id,
                    'user_name'         =>$q->user->name,
                    'main_category_id'       =>$q->main_category_id,
                    'main_category'       =>$q->main_category->title,
                    'category_id'       =>$q->category_id,
                    'category'          =>$q->category->title,
                    'car_number'        =>$q->car_number,
                    'lat'               =>$q->lat,
                    'long'              =>$q->long,
                    'online'              =>$q->online,
                    'national_image'    =>getImg($q->national_image),
                    'licence_image'     =>getImg($q->licence_image),
                    'st_image'          =>getImg($q->st_image),
                    'copy_image'        =>getImg($q->copy_image),
                    'front_image'       =>getImg($q->front_image),
                    'back_image'        =>getImg($q->back_image),
                    'created_at'              =>$q->created_at,
                ];
            }),
            'paginate'=>[
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'next_page_url'=>$this->nextPageUrl(),
                'prev_page_url'=>$this->previousPageUrl(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ]
          ];
        //return parent::toArray($request);
    }
}
