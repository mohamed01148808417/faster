<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AccountsResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'accounts'=>$this->collection->transform(function ($q){
                return [
                    'id'                =>$q->id,
                    'bank_id'                =>$q->bank_id,
                    'bank_name'                =>$q->bank_name,
                    'account_IBAN'                =>$q->account_IBAN,
                    'created_at'        =>$q->created_at,
                ];
            }),
            'paginate'=>[
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'next_page_url'=>$this->nextPageUrl(),
                'prev_page_url'=>$this->previousPageUrl(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ]
          ];

        //return parent::toArray($request);
    }
}
