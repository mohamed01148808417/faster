<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClickResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'clicks'=>$this->collection->transform(function ($q){
                return [
                    'id'            =>$q->id,
                    'user_id'            =>$q->user_id,
                    'user_name'            =>$q->user->name,
                    'mobile_click'            =>$q->mobile_click,
                    'whatsapp_click'            =>$q->whatsapp_click
                ];
            }),
            'paginate'=>[
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'next_page_url'=>$this->nextPageUrl(),
                'prev_page_url'=>$this->previousPageUrl(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ]
          ];
        //return parent::toArray($request);
    }
}
