<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'users'=>$this->collection->transform(function ($q){
                return [
                    'id'                =>$q->id,
                    'name'              =>$q->name,
                    'phone'             =>$q->phone,
                    'image'             =>getImg($q->image),
                    'email'             =>$q->email,
                    'verified'          =>$q->verified,
                    'city_id'           =>$q->city_id,
                    'city'              =>$q->city->title,
                    'lat'               =>$q->lat,
                    'long'              =>$q->long,
                    'points'            =>$q->points,
                    'fcm_token'         =>$q->fcm_token,
                    'main_category'       =>($q->user_type == 2 )? $q->main_category : null,
                    'cars'              =>($q->user_type == 2 )? $q->cars : 'not have cars it is user ' ,
                    'user_type'         =>$q->user_type,

                ];
            }),
            'paginate'=>[
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'next_page_url'=>$this->nextPageUrl(),
                'prev_page_url'=>$this->previousPageUrl(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ]
          ];
        //return parent::toArray($request);
    }
}
