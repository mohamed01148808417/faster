<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SettingResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'settings'=>$this->collection->transform(function ($q){
                return [
                    'url'          =>$q->url,
                    'terms'          =>$q->terms,
                    'app_name'          =>$q->app_name,
                    'app_token'          =>$q->app_token,
                    'contact_number'          =>$q->contact_number,
                    'contact_email'          =>$q->contact_email,
                ];
            }),
            'paginate'=>[
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'next_page_url'=>$this->nextPageUrl(),
                'prev_page_url'=>$this->previousPageUrl(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ]
          ];
        //return parent::toArray($request);
    }
}
