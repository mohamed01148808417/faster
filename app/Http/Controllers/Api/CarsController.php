<?php

namespace App\Http\Controllers\Api;

use App\Car;
use App\Http\Resources\CarsResource;
use App\Http\Traits\ApiResponses;

use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Carbon\Carbon;
class CarsController extends Controller
{

    use ApiResponses;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
        $cars = Car::where('online',1)->get();
        $data = [];
        foreach ($cars as $car){

            $data[] = [
                'id'                =>$car->id,
                'user_id'           =>$car->user_id,
                'user_name'         =>$car->user->name,
                'user_phone'         =>$car->user->phone,
                'user_image'         =>getImg($car->user->image),
                'main_category_id'       =>$car->category_id,
                'main_category'       =>$car->main_category->title,
                'category_id'       =>$car->category_id,
                'category'          =>$car->category->title,
                'car_number'        =>$car->car_number,
                'lat'               =>$car->lat,
                'long'              =>$car->long,
                'online'              =>$car->online,
                'national_image'    =>getImg($car->national_image),
                'licence_image'     =>getImg($car->licence_image),
                'st_image'          =>getImg($car->st_image),
                'copy_image'        =>getImg($car->copy_image),
                'front_image'       =>getImg($car->front_image),
                'back_image'        =>getImg($car->back_image),
                'created_at'              =>$car->created_at,
            ];

        }

        $data = [
            'value' => true,
            'data'  => [
                'cars'  => $data
            ],
        ];

        return response()->json($data,200);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        //dd($request->all());

        $user = JWTAuth::user();


        if(!$user){

            return response()->json([
                'success' => false,
                'message' => 'please login '
                ]
                ,400);
        }

        $validator = Validator::make($request->all(), [
            'category' => 'required',
            'main_category' => 'required',
            'car_number' => 'required',
            'national_image' => 'required|image',
            'licence_image' => 'required|image',
            'st_image' => 'required|image',
            'copy_image' => 'required|image',
            'front_image' => 'required|image',
            'back_image' => 'required|image',
            'lat' => 'required',
            'long' => 'required',
        ]);




        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $national_image = uploader($request->file('national_image'));
        $licence_image = uploader($request->file('licence_image'));
        $st_image = uploader($request->file('st_image'));
        $copy_image = uploader($request->file('copy_image'));
        $front_image = uploader($request->file('front_image'));
        $back_image = uploader($request->file('back_image'));



        $car = new Car();

        $car->user_id = $user->id;
        $car->category_id = $request->get('category');
        $car->main_category_id = $request->get('main_category');
        $car->car_number = $request->get('car_number');
        $car->national_image = $national_image;
        $car->licence_image = $licence_image;
        $car->st_image = $st_image;
        $car->copy_image = $copy_image;
        $car->front_image = $front_image;
        $car->back_image = $back_image;
        $car->lat = $request->get('lat');
        $car->long = $request->get('long');
        $car->online = 1;
        $car->created_at = Carbon::now();


        if($car->save()){

            return response()->json('created car successfully ',200);

        }else{
            return response()->json('error to save ',400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $car_id
     * @return \Illuminate\Http\Response
     */
    public function show($car_id)
    {
        //

        $car = Car::findOrFail($car_id);


        if($car){

            $user_id = $car->user_id;

            $user = User::findOrFail($user_id);

            $user = [
                'id'                =>$user->id,
                'name'              =>$user->name,
                'phone'             =>$user->phone,
                'image'             =>getImg($user->image),
                'email'             =>$user->email,
                'verified'          =>$user->verified,
                'city_id'           =>$user->city_id,
                'city'              =>$user->city->title,
                'lat'               =>$user->lat,
                'long'              =>$user->long,
                'points'            =>$user->points,
                'fcm_token'         =>$user->fcm_token,
                'main_category'          =>($user->user_type == 2)? $user->main_category : null,
                'cars'              =>($user->user_type == 2 )? $user->cars : 'not have cars it is user ' ,
                'user_type'         =>$user->user_type,
            ];


            $data = [
                'value' => true,
                'data'  => [
                    'user'  => $user
                ]
            ];

            return response()->json($data,200);

        }else{
            return response()->json('not found car',400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function updateLocation(Request $request)
    {
        //
        $car = Car::findOrFail($request->get('car_id'));

        if($car){
            $car->lat = $request->get('lat');
            $car->long = $request->get('long');

            if($car->save()){
                return response()->json('Location Updated',201);
            }else{
                return response()->json('not updated location ',400);
            }
        }
    }

    /*
     * get car based on main_categories id
     *
     * */
    public function getCarsBasedOnMainCategory($main_category_id){

        $cars = Car::where('main_category_id',$main_category_id)->orderBy('id','DESC')->get();
        $data = [];
        if($cars){


            foreach ($cars as $car){

                $data[] = [
                    'id'                =>$car->id,
                    'user_id'           =>$car->user_id,
                    'user_name'         =>$car->user->name,
                    'user_phone'         =>$car->user->phone,
                    'user_image'       =>getImg($car->user->image),
                    'main_category_id'       =>$car->category_id,
                    'main_category'       =>$car->main_category->title,
                    'category_id'       =>$car->category_id,
                    'category'          =>$car->category->title,
                    'car_number'        =>$car->car_number,
                    'lat'               =>$car->lat,
                    'long'              =>$car->long,
                    'online'              =>$car->online,
                    'national_image'    =>getImg($car->national_image),
                    'licence_image'     =>getImg($car->licence_image),
                    'st_image'          =>getImg($car->st_image),
                    'copy_image'        =>getImg($car->copy_image),
                    'front_image'       =>getImg($car->front_image),
                    'back_image'        =>getImg($car->back_image),
                    'created_at'              =>$car->created_at,
                ];

            }

            $data = [
                'value' => true,
                'data'  => [
                    'cars'  => $data
                ],
            ];

            return response()->json($data,200);

        }else{
            return response()->json('not found cars yet',200);
        }
    }

    public function offline($car_id){

        $user = JWTAuth::user();
        if(!$user){

            return response()->json([
                    'success' => false,
                    'message' => 'please login '
                ]
                ,400);
        }

        $car = Car::where('id',$car_id)->where('user_id',$user->id)->first();
        if($car){
            $car->online = 0;
            if($car->save()){
                return response()->json([
                        'success' => false,
                        'message' => 'offline car okay'
                    ]
                    ,200);
            }else{
                return response()->json([
                        'success' => false,
                        'message' => 'car not saved'
                    ]
                    ,400);
            }
        }else{
            return response()->json([
                    'success' => false,
                    'message' => 'this car not for you'
                ]
                ,400);
        }
    }
    public function online($car_id){

        $user = JWTAuth::user();
        if(!$user){

            return response()->json([
                    'success' => false,
                    'message' => 'please login '
                ]
                ,400);
        }

        $car = Car::where('id',$car_id)->where('user_id',$user->id)->first();
        if($car){
            $car->online = 1;
            if($car->save()){
                return response()->json([
                        'success' => false,
                        'message' => 'online car okay'
                    ]
                    ,200);
            }else{
                return response()->json([
                        'success' => false,
                        'message' => 'car not saved'
                    ]
                    ,400);
            }
        }else{
            return response()->json([
                    'success' => false,
                    'message' => 'this car not for you'
                ]
                ,400);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
