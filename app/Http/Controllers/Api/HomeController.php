<?php

namespace App\Http\Controllers\Api;

use App\Account;
use App\Category;
use App\City;
use App\Contact;
use App\Http\Controllers\MainCategoryController;
use App\Http\Resources\AccountsResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CityResource;
use App\Http\Resources\MainCategoryResource;
use App\Http\Resources\SettingResource;
use App\Http\Traits\ApiResponses;
use App\MainCategory;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


class HomeController extends Controller
{
    //
    use ApiResponses;

    /*
     * get all categories
     * */


    public function getCategories(){

        $categories = Category::paginate(10);

        $categories = new CategoryResource($categories);
        return $this->apiResponse($categories);

    }

    //


    /*
     * get all Cities
     * */

    public function getCities(){

        $cities = City::paginate(10);

        $cities = new CityResource($cities);
        return $this->apiResponse($cities);

    }


    /*
     * get all Accounts
     * */

    public function getAccounts(){

        $accounts = Account::paginate(10);

        $accounts = new AccountsResource($accounts);
        return $this->apiResponse($accounts);

    }

    /*
     * get all Accounts
     * */

    public function getMainCategories(){

        $main_categories = MainCategory::paginate(10);

        $main_categories = new MainCategoryResource($main_categories);
        return $this->apiResponse($main_categories);

    }

    /*
     * get settings
     * */

    public function getSetting(){

        $settings = Setting::paginate(1);

        $settings = new SettingResource($settings);
        return $this->apiResponse($settings);

    }

    /*
     * contacts function
     * **/
    public function contacts(Request $request){

        //$user = JWTAuth::user();

        $validator = Validator::make($request->all(),[
            'name'  => 'required',
            'phone' => 'required',
            'message' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $contact = new Contact();

        $contact->name = $request->get('name');
        $contact->phone = $request->get('phone');
        $contact->message = $request->get('message');
        $contact->created_at    = Carbon::now();

        if($contact->save()){
            return response()->json('sending message okay admin will check it',200);
        }else{
            return response()->json('please send true data ',400);
        }
    }

}
