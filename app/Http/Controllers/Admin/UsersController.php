<?php

namespace App\Http\Controllers\Admin;

use App\Car;
use App\Category;
use App\City;
use App\MainCategory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{


    public function login(){
        return view('admin.users.login');
    }
    public function doLogin(Request $request){
        $email = $request->get('email');
        $password = $request->get('password');

        // check is true user is admin
        if (Auth::attempt(['email' => $email, 'password' => $password]) && Auth::user()->user_type == 3) {
            return view('admin.dashboard.index');
        }else{
            return redirect()->back()->with('error','you not admin');
        }
    }


    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        if(isset($_GET['user_type'])){
            if(!empty($_GET['user_type'])){
                $users = User::where('user_type',$_GET['user_type'])->orderBy('id','DESC')->paginate(50);
                return view('admin.users.index')->with('users',$users);
            }else{
                $users = User::where('user_type','!=',3)->orderBy('id','DESC')->paginate(50);
                return view('admin.users.index')->with('users',$users);
            }
        }else{

            $users = User::where('user_type','!=',3)->orderBy('id','DESC')->paginate(50);
            return view('admin.users.index')->with('users',$users);
        }





    }

    public function userCars($user_id){
        $mainCategories =  MainCategory::pluck('title', 'id')->toArray();
        $users = Car::where('user_id',$user_id)->orderBy('id','DESC')->paginate(50);
        return view('admin.cars.index')->with('cars',$users)->with('main_categories',$mainCategories);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $categories =  MainCategory::pluck('title', 'id')->toArray();
        $cities = City::pluck('title', 'id')->toArray();

        return view('admin.users.create')->with('categories',$categories)->with('cities',$cities);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        $validator = \Validator::make($request->all(),[
            'name'=>'required',
            'email'=>'required|email|unique:users',
            'phone'=>'required|unique:users',
            'password' => 'required|confirmed|min:6',
            'lat'=>'required',
            'long'=>'required',
            'city'=>'required',
            'user_type'=>'required',
            'image'=>'required|mimes:jpeg,jpg,bmp,png|max:2048'
        ]);

        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');

            return back();
        }

        $user = new User();

        if ($request->has(['password']))
        {
            $user->password = Hash::make($request->password);
        }

        if ($request->has('image'))
        {
            $user->image = uploader($request->image);
        }


        if($request->user_type == 1){
            $user->name = $request->name;
            $user->phone= $request->phone;
            $user->email = $request->email;
            $user->user_type = 1;
            $user->city_id = $request->city;
            $user->lat = $request->lat;
            $user->long = $request->long;
            $user->verified = 1;
            $user->created_at = Carbon::now();
        }



        if($request->user_type == 2){


            $validator = \Validator::make($request->all(),[

                'main_category'=>'required',
                'points'=>'required'
            ]);

            if ($validator->fails())
            {
                session()->flash('error','خطأ في البيانات');

                return back();
            }

            $user->name = $request->name;
            $user->phone= $request->phone;
            $user->email = $request->email;
            $user->user_type = 2;
            $user->main_category_id = $request->main_category;
            $user->points = $request->points;
            $user->city_id = $request->city;
            $user->lat = $request->lat;
            $user->long = $request->long;
            $user->verified = 1;
            $user->created_at = Carbon::now();
        }






        if($user->save()){

            session()->flash('success','تم الحفظ  ');

        }

        return back();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        $categories =  MainCategory::pluck('title', 'id')->toArray();
        $cities = City::pluck('title', 'id')->toArray();


        $user = User::findOrFail($id);
        return view('admin.users.edit')
            ->with('user',$user)
            ->with('cities',$cities)
            ->with('categories',$categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user =  User::findOrFail($id);


        //dd($request->all());
        $validator = \Validator::make($request->all(),[
            'name'=>'required',
            'email'=>'required|email|unique:users,email,'.$user->id,
            'phone'=>'required|unique:users,phone,'.$user->id,
            'lat'=>'required',
            'long'=>'required',
            'city'=>'required',
        ]);


        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');

            return back();
        }


        if ($request->has('image'))
        {
            $validator = \Validator::make($request->all(),[
                'image'=>'required|mimes:jpeg,jpg,bmp,png|max:2048'
            ]);

            if ($validator->fails())
            {
                session()->flash('error','خطأ في البيانات');

                return back();
            }

            $user->image = uploader($request->image);
        }



        $user->name = $request->name;
        $user->phone= $request->phone;
        $user->email = $request->email;
        $user->city_id = $request->city;
        $user->lat = $request->lat;
        $user->long = $request->long;
        $user->updated_at = Carbon::now();






        if($user->save()){

            session()->flash('success','تم التعديل  ');

        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
