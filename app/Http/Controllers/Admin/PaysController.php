<?php

namespace App\Http\Controllers\Admin;

use App\Pay;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $pays = Pay::paginate(50);
        return view('admin.pays.index')->with('pays',$pays);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //

        $pay = Pay::findOrFail($id);

        if($pay->delete()){
            session()->flash('success','تم المسح');
            return back();
        }
    }
}
