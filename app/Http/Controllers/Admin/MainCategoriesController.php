<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\MainCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = MainCategory::paginate(50);
        return view('admin.main_categories.index')->with('categories',$categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.main_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = \Validator::make($request->all(),[
            'title'=>'required'
        ]);

        $category = new MainCategory();


        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');

            return back();
        }

        $category->title = $request->title;
        if($category->save()){
            session()->flash('success','تم الحفظ');

            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category = MainCategory::findOrFail($id);
        return view('admin.main_categories.edit')->with('category',$category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = \Validator::make($request->all(),[
            'title'=>'required'
        ]);

        $category = MainCategory::findOrFail($id);

        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');

            return back();
        }

        $category->title = $request->title;
        if($category->save()){
            session()->flash('success','تم التعديل');

            return back();
        }else{
            session()->flash('success','لم يتم التعديل');

            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $category = MainCategory::findOrFail($id);

        if($category->delete()){
            session()->flash('success','تم المسح');

            return back();
        }
    }
}
