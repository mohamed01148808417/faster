<?php

namespace App\Http\Controllers\Admin;

use App\Car;
use App\Category;
use App\City;
use App\MainCategory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CarsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $mainCategories =  MainCategory::pluck('title', 'id')->toArray();
        if(isset($_GET['main_category_id'])){

            if($_GET['main_category_id'] && !empty($_GET['main_category_id'])){

                $users = Car::where('main_category_id',$_GET['main_category_id'])->orderBy('id','DESC')->paginate(50);

                return view('admin.cars.index')->with('cars',$users) ->with('main_categories',$mainCategories);

            }


            $users = Car::orderBy('id','DESC')->paginate(50);

            return view('admin.cars.index')->with('cars',$users) ->with('main_categories',$mainCategories);
        }else{
            $users = Car::orderBy('id','DESC')->paginate(50);

            return view('admin.cars.index')->with('cars',$users) ->with('main_categories',$mainCategories);
        }





    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $users = User::where('user_type',2)->orderBy('id','desc')->pluck('name','id')->toArray();
        $categories =  Category::pluck('title', 'id')->toArray();

        $mainCategories =  MainCategory::pluck('title', 'id')->toArray();
        $cities = City::pluck('title', 'id')->toArray();

        return view('admin.cars.create')
            ->with('categories',$categories)
            ->with('cities',$cities)
            ->with('main_categories',$mainCategories)
            ->with('users',$users);
        
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(),[
            'car_number'=>'required',
            'user_id'=>'required',
            'category_id'=>'required',
            'main_category_id'=>'required',
            'national_image'=>'required|mimes:jpeg,jpg,bmp,png',
            'licence_image'=>'required|mimes:jpeg,jpg,bmp,png',
            'st_image'=>'required|mimes:jpeg,jpg,bmp,png',
            'copy_image'=>'required|mimes:jpeg,jpg,bmp,png',
            'front_image'=>'required|mimes:jpeg,jpg,bmp,png',
            'back_image'=>'required|mimes:jpeg,jpg,bmp,png',
        ]);



        if ($validator->fails())
        {

            session()->flash('error','خطأ في البيانات');

            return back();
        }

        $car = new Car();


        $car->user_id = $request->get('user_id');
        $car->car_number = $request->get('car_number');
        $car->category_id = $request->get('category_id');
        $car->main_category_id = $request->get('main_category_id');
        $car->national_image = uploader($request->national_image);
        $car->licence_image = uploader($request->licence_image);
        $car->st_image = uploader($request->st_image);
        $car->copy_image = uploader($request->copy_image);
        $car->front_image = uploader($request->front_image);
        $car->back_image = uploader($request->back_image);
        $car->lat = $request->lat;
        $car->long = $request->long;
        $car->created_at = Carbon::now();





        if($car->save()){

            session()->flash('success','تم الحفظ  ');
        }

        return back();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $car = Car::findOrFail($id);
        if($car){
            return view('admin.cars.show')->with('car',$car);
        }else{
            return redirect()->back();
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::where('user_type',2)->orderBy('id','desc')->pluck('name','id')->toArray();
        $categories =  Category::pluck('title', 'id')->toArray();

        $mainCategories =  MainCategory::pluck('title', 'id')->toArray();
        $cities = City::pluck('title', 'id')->toArray();

        return view('admin.cars.edit')->with('car',Car::findOrFail($id))
            ->with('categories',$categories)
            ->with('cities',$cities)
            ->with('main_categories',$mainCategories)
            ->with('users',$users);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //dd($request->all());
        $request->validate([
            'car_number'=>'required',
            'user_id'=>'required',
            'category_id'=>'required',
            'main_category_id'=>'required'

        ]);

        $car = Car::findorFail($id);

        $validator = \Validator::make($request->all(),[
            'car_number'=>'required',
            'user_id'=>'required',
            'category_id'=>'required',
            'main_category_id'=>'required'
        ]);




        if ($validator->fails())
        {

            session()->flash('error','خطأ في البيانات');

            return back();
        }



        if($request->national_image){
            $validator = \Validator::make($request->all(),[
                'national_image'=>'required|mimes:jpeg,jpg,bmp,png',
            ]);
            if ($validator->fails())
            {

                session()->flash('error','خطأ في البيانات');

                return back();
            }
            $car->national_image = uploader($request->national_image);
        }

        if($request->licence_image){
            $validator = \Validator::make($request->all(),[
                'licence_image'=>'required|mimes:jpeg,jpg,bmp,png',
            ]);
            if ($validator->fails())
            {

                session()->flash('error','خطأ في البيانات');

                return back();
            }
            $car->licence_image = uploader($request->licence_image);
        }

        if($request->st_image){
            $validator = \Validator::make($request->all(),[
                'st_image'=>'required|mimes:jpeg,jpg,bmp,png',
            ]);
            if ($validator->fails())
            {

                session()->flash('error','خطأ في البيانات');

                return back();
            }
            $car->st_image = uploader($request->st_image);
        }

        if($request->copy_image){
            $validator = \Validator::make($request->all(),[
                'copy_image'=>'required|mimes:jpeg,jpg,bmp,png',
            ]);
            if ($validator->fails())
            {

                session()->flash('error','خطأ في البيانات');

                return back();
            }
            $car->copy_image = uploader($request->copy_image);
        }

        if($request->front_image){
            $validator = \Validator::make($request->all(),[
                'front_image'=>'required|mimes:jpeg,jpg,bmp,png',
            ]);
            if ($validator->fails())
            {

                session()->flash('error','خطأ في البيانات');

                return back();
            }
            $car->front_image = uploader($request->front_image);
        }

        if($request->back_image){
            $validator = \Validator::make($request->all(),[
                'back_image'=>'required|mimes:jpeg,jpg,bmp,png',
            ]);
            if ($validator->fails())
            {

                session()->flash('error','خطأ في البيانات');

                return back();
            }
            $car->back_image = uploader($request->back_image);
        }

        $car->user_id = $request->get('user_id');
        $car->car_number = $request->get('car_number');
        $car->category_id = $request->get('category_id');
        $car->main_category_id = $request->get('main_category_id');
        $car->lat = $request->lat;
        $car->long = $request->long;
        $car->updated_at = Carbon::now();


        if($car->save()){

            session()->flash('success','تم التعديل  ');
        }

        return back();



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $car = Car::findOrFail($id);
        if($car->delete()){
            session()->flash('success','تم مسح العربة  ');

            return back();
        }
    }
}
