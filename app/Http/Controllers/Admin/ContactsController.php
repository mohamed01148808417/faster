<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $contacts = Contact::paginate(50);
        return view('admin.contacts.index')->with('contacts',$contacts);
    }

    public function destroy($id)
    {

        $account = Contact::findOrFail($id);
        if($account->delete()){
            session()->flash('success','تم المسح');
            return back();
        }
    }
}
