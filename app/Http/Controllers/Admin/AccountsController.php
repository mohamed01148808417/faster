<?php

namespace App\Http\Controllers\Admin;

use App\Account;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $accounts = Account::paginate(50);
        return view('admin.accounts.index')->with('accounts',$accounts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.accounts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = \Validator::make($request->all(),[
            'bank_id'=>'required',
            'bank_name'=>'required',
            'account_iban'=>'required',
        ]);

        $account = new Account();


        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');

            return back();
        }

        $account->bank_id = $request->bank_id;
        $account->bank_name = $request->bank_name;
        $account->account_IBAN = $request->account_iban;
        if($account->save()){
            session()->flash('success','تم الحفظ');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $account = Account::findOrFail($id);
        return view('admin.accounts.edit')->with('account',$account);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = \Validator::make($request->all(),[
            'bank_id'=>'required',
            'bank_name'=>'required',
            'account_iban'=>'required',
        ]);

        $account = Account::findOrFail($id);

        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');

            return back();
        }

        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');

            return back();
        }

        $account->bank_id = $request->bank_id;
        $account->bank_name = $request->bank_name;
        $account->account_IBAN = $request->account_iban;
        if($account->save()){
            session()->flash('success','تم التعديل');
            return back();
        }else{
            session()->flash('success','لم يتم التعديل');

            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $account = Account::findOrFail($id);

        if($account->delete()){
            session()->flash('success','تم المسح');
            return back();
        }
    }
}
