<?php

namespace App\Http\Controllers;

use App\Car;
use App\Chat;
use App\Click;
use App\Http\Resources\CarsResource;
use App\Http\Resources\ChatMessageResource;
use App\Http\Resources\ChatResource;
use App\Http\Resources\ClickResource;
use App\Http\Resources\UserResource;
use App\Http\Traits\ApiResponses;

use App\Pay;
use App\User;
use Carbon\Carbon;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Namshi\JOSE\JWT;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    use ApiResponses;

    /*
     * Login data
     * */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('phone', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = User::where('phone',$request->get('phone'))->first();

        $user = [
            'id'                =>$user->id,
            'name'              =>$user->name,
            'phone'             =>$user->phone,
            'image'             =>getImg($user->image),
            'email'             =>$user->email,
            'verified'          =>$user->verified,
            'city_id'           =>$user->city_id,
            'city'              =>($user->city_id !== null )? $user->city->title : null,
            'lat'               =>$user->lat,
            'long'              =>$user->long,
            'points'            =>$user->points,
            'fcm_token'         =>$user->fcm_token,
            'main_category_id'       =>($user->user_type == 2 )? $user->main_category_id : null,
            'main_category'          =>($user->user_type == 2)? $user->main_category->title : null,
            'cars'              =>($user->user_type == 2 )? $user->cars : 'not have cars it is user ' ,
            'user_type'         =>$user->user_type,
        ];


        return response()->json(compact('user','token'));
    }

    /*
     *
     * Register data
     * */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'nullable|string|max:255',
            'phone' => 'required|string|numeric|unique:users',
            'email' => 'nullable|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'type' => 'required|int',
            'main_category'  => 'nullable|int',
            'lat'  => 'nullable|numeric',
            'long'  => 'nullable|numeric',
            'image'=>'nullable|mimes:jpeg,jpg,bmp,png',
            'city'  => 'nullable|int',
        ]);



        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }


        if($request->type == 1){

            if($request->hasFile('image')){
                $user = User::create([
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'phone' => $request->get('phone'),
                    'city_id' => $request->get('city'),
                    'user_type' => 1,
                    'lat' => $request->get('lat'),
                    'long' => $request->get('long'),
                    'main_category_id' => null,
                    'image' => uploader($request->image),
                    'password' => Hash::make($request->get('password')),
                ]);
            }else{
                $user = User::create([
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'phone' => $request->get('phone'),
                    'city_id' => $request->get('city'),
                    'user_type' => 1,
                    'lat' => $request->get('lat'),
                    'long' => $request->get('long'),
                    'main_category_id' => null,
                    'password' => Hash::make($request->get('password')),
                ]);
            }

            $token = JWTAuth::fromUser($user);


            $user = [
                'id'                =>$user->id,
                'name'              =>$user->name,
                'phone'             =>$user->phone,
                'image'             =>getImg($user->image),
                'email'             =>$user->email,
                'verified'          =>$user->verified,
                'city_id'           =>$user->city_id,
                'city'              =>($user->city_id !== null )? $user->city->title : null,
                'lat'               =>$user->lat,
                'long'              =>$user->long,
                'points'            =>$user->points,
                'fcm_token'         =>$user->fcm_token,
                'main_category_id'  =>($user->user_type == 2 )? $user->main_category : null,
                'cars'              =>($user->user_type == 2 )? $user->cars : 'not have cars it is user ' ,
                'user_type'         =>$user->user_type,
            ];


            return response()->json(compact('user','token'),201);

        }
        elseif ($request->type == 2){

            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'phone' => 'required|string|numeric|unique:users',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6',
                'type' => 'required|int',
                'main_category'  => 'required|int',
                'lat'  => 'required|numeric',
                'long'  => 'required|numeric',
                'city'  => 'required|int',
                'image'=>'required|mimes:jpeg,jpg,bmp,png',
            ]);


            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }
            $user = User::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'city_id' => $request->get('city'),
                'points' => 500,
                'image' => uploader($request->image),
                'user_type' => 2,
                'lat' => $request->get('lat'),
                'long' => $request->get('long'),
                'main_category_id' => $request->get('main_category'),
                'password' => Hash::make($request->get('password')),
            ]);


            Click::create([
                'user_id' => $user->id,
                'mobile_click' =>0,
                'whatsapp_click' =>0,
                'created_at' =>Carbon::now(),
            ]);


            $token = JWTAuth::fromUser($user);

            $user = [
                'id'                =>$user->id,
                'name'              =>$user->name,
                'phone'             =>$user->phone,
                'image'             =>getImg($user->image),
                'email'             =>$user->email,
                'verified'          =>$user->verified,
                'city_id'           =>$user->city_id,
                'city'              =>($user->city_id !== null )? $user->city->title : null,
                'lat'               =>$user->lat,
                'long'              =>$user->long,
                'points'            =>$user->points,
                'fcm_token'         =>$user->fcm_token,
                'main_category'          =>($user->user_type == 2)? $user->main_category : null,
                'cars'              =>($user->user_type == 2 )? $user->cars : 'not have cars it is user ' ,
                'user_type'         =>$user->user_type,
            ];

            return response()->json(compact('user','token'),201);
        }
        else{
            return response()->json('error you not send the type ', 400);
        }


    }

    /*
     * get authenticate user
     * */
    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }


    /*
     * update data for user
     * */
    public function update(Request $request){
        $user = JWTAuth::user();

        $user_id = $user->id;
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$user_id,
            'lat'  => 'nullable|numeric',
            'long'  => 'nullable|numeric',
            'city'  => 'required|int',
        ]);


        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        if($request->hasFile('image') && !empty($request->file('image'))){

            // update user with image

            //$file = $request->file('image');

            $imgName = uploader($request->file('image'));


            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->lat = $request->get('lat');
            $user->long = $request->get('long');
            $user->city_id = $request->get('city');
            $user->image = $imgName;

            if($user->save()){
                $user = User::findOrFail($user_id);
                $user = [
                    'id'                =>$user->id,
                    'name'              =>$user->name,
                    'phone'             =>$user->phone,
                    'image'             =>getImg($user->image),
                    'email'             =>$user->email,
                    'verified'          =>$user->verified,
                    'city_id'           =>$user->city_id,
                    'city'              =>($user->city_id !== null )? $user->city->title : null,
                    'lat'               =>$user->lat,
                    'long'              =>$user->long,
                    'fcm_token'         =>$user->fcm_token,
                ];

                return response()->json(compact('user'),200);
            }else{
                return response()->json('Error Updating user');
            }


        }else{
            //update the user without image

            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->lat = $request->get('lat');
            $user->long = $request->get('long');
            $user->city_id = $request->get('city');

            if($user->save()){
                $user = User::findOrFail($user_id);
                $user = [
                    'id'                =>$user->id,
                    'name'              =>$user->name,
                    'phone'             =>$user->phone,
                    'image'             =>getImg($user->image),
                    'email'             =>$user->email,
                    'verified'          =>$user->verified,
                    'city_id'           =>$user->city_id,
                    'city'              =>($user->city_id !== null )? $user->city->title : null,
                    'lat'               =>$user->lat,
                    'long'              =>$user->long,
                    'fcm_token'         =>$user->fcm_token,
                ];

                return response()->json(compact('user'),200);
            }else{
                return response()->json('Error Updating user');
            }
        }

        //return response()->json(compact('user','token'),201);

    }

    /*
     * update old password
     * */
    public function updatePassword(Request $request){

        $user = JWTAuth::user();
        $validator = Validator::make($request->all(), [
            'old_pass' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);


        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $old_password = $request->get('old_pass');


        if ((Hash::check($old_password, $user->password))) {
            // The passwords matches
            $user->password =  Hash::make($request->get('password'));
            $user->save();

            return response()->json([
                'success' => 'Updated password successfully'
            ],'200');

        }else{
            return response()->json([
                'error' => 'Your current password does not matches with the password you provided. Please try again'
            ],'400');
        }
    }

    public function all(){
        $users = User::paginate(10);
        $users = new UserResource($users);
        return $this->apiResponse($users);
    }


    public function userCars(){
        $user = JWTAuth::user();

        $cars = Car::where('user_id',$user->id)->orderBy('created_at','desc')->paginate(20);
        $cars = new CarsResource($cars);

        return $this->apiResponse($cars);

    }

    public function getCars(){

        $cars = Car::orderBy('created_at','desc')->paginate(20);
        $cars = new CarsResource($cars);
        return $this->apiResponse($cars);

    }



    public function mobileClick(Request $request){

        $authUser = JWTAuth::user();


        //$provider = $authUser->id;

        // update user and and add one click

        $provider_user = $request->get('provider_user');

        if($provider_user == $authUser->id){
            return response()->json('you can not increase the mobile click',200);
        }

        $click = Click::where('user_id',$provider_user)->first();


        $mobile_click = $click->mobile_click;

        $increment =  intval($mobile_click + 1);

        $click->mobile_click = ($increment);

        if($click->save()){
            return response()->json('increment user view true',200);
        }else{
            return response()->json('error increment ',400);
        }
    }

    public function whatsappClick(Request $request){

        $authUser = JWTAuth::user();

        $provider_user = $request->get('provider_user');
        //$provider = $authUser->id;

        // update user and and add one click

        if($provider_user == $authUser->id){
            return response()->json('you can not increase the mobile click',200);
        }

        $click = Click::where('user_id',$provider_user)->first();

        $whatsapp_click = $click->whatsapp_click;

        $increment =  $whatsapp_click + 1;

        $click->whatsapp_click = $increment;

        if($click->save() ){
            return response()->json('increment user view true',200);
        }else{
            return response()->json('error increment ',400);
        }

    }

    public function clicks($user_id){

        // get all clicks
        $clicks = Click::where('user_id',$user_id)->first();

        $clicks = [

            'data' => [

                'id'    => $clicks->id,
                'user_id'    => $clicks->user_id,
                'mobile_click'    => intval($clicks->mobile_click),
                'whatsapp_click'    => intval($clicks->whatsapp_click),
                'updated_at'        => $clicks->updated_at
            ],

        ];

        return response()->json($clicks,'200');

    }



    public function my_chats(){
        $user = JWTAuth::user();

        $chats_users = Chat::select('id','sender','receiver',
            'message','has_file')->where('sender',$user->id)
            ->orWhere('receiver',$user->id)
            ->groupBy('receiver')->distinct()
            ->groupBy('sender')->distinct()
            ->paginate(50);
        //dd($chats_users);

        $users = new ChatResource($chats_users);

        return $this->apiResponse($users);
    }

    public function chatWithUser($user_id){
        $user = JWTAuth::user();
        $user2 = $user->id;
        $chats_users = Chat::where('sender','=',$user2)->where('receiver','=',$user_id)
            ->orWhere('sender','=',$user_id)->where('receiver','=',$user2)->orderBy('created_at')->paginate(10);
        //       $chats_users = DB::select("SELECT * FROM chats WHERE (sender = '$user->id' AND receiver = '$user_id' ) OR (sender = '$user_id' AND receiver = '$user->id')");
        $users = new ChatMessageResource($chats_users);

        return $this->apiResponse($users);
    }

    public function sendMessageToUser(Request $request){
        $chat = new Chat();
        $sender = JWTAuth::user();


        $validator = Validator::make($request->all(), [
            'user_id' => 'required|int',
            'message' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $message = $request->get('message');
        $user_id = $request->get('user_id');
        $receiver = User::findOrFail($user_id);

        // you can send a message
        if($request->hasFile('attachment') && !empty($request->file('attachment'))){
            $file = $request->file('attachment');
            //$filename = $file->getClientOriginalName();
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $path = public_path().'/uploads/';

            $file->move($path, $filename);
            $attachment = $filename;

        }else{
            $attachment = null;
        }

        $chat->message = $message;
        $chat->sender = $sender->id;
        $chat->receiver = $receiver->id;
        $chat->has_file = $attachment;
        $chat->created_at = today()->toDateTimeString();
        $chat->updated_at = null;
        if($chat->save()){
            return response()->json([
                'success' => 'Add message successfully'
            ],'200');
        }else{
            return response()->json([
                'error' => 'error to inserting chat '
            ],'200');

        }


    }

    /*
     *
     * pay
     * */
    public function pay(Request $request){

        $user = JWTAuth::user();
        $user_id =$user->id;

        $validator = Validator::make($request->all(), [
            'image'  => 'required|image',
        ]);


        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $image = uploader($request->file('image'));


        $pay = new Pay();
        $pay->user_id = $user_id;
        $pay->image   = $image;
        $pay->created_at = Carbon::now();

        if($pay->save()){
            return response()->json('sending pay is okay admin will check it',200);
        }else{
            return response()->json('sending pay is false please resend it',400);
        }


    }
}