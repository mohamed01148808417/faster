<?php
const URL = 'http://127.0.0.1:8000/';
function getImg($name){

    if(!null == $name){
        return  URL . 'uploads/' . $name;
    }else{
        return null;
    }
}
function uploader($file){
    $fileName = $file->getClientOriginalName();
    $file->move(public_path('uploads'),time() .'image_'.'_'.$fileName);
    return  time() .'image_'.'_'.$fileName;
}
