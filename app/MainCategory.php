<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainCategory extends Model
{
    //
    protected $fillable = ['id','title'];

    public function users(){
        return $this->hasMany('App\User','main_category_id','id');
    }
    public function cars(){
        return $this->hasMany('App\Car','main_category_id','id');
    }
}
