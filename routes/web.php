<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('admin/login','Admin\UsersController@login')->name('show_login');
Route::post('admin/login','Admin\UsersController@doLogin')->name('do_login');
Route::get('admin/logout','Admin\UsersController@logout')->name('logout');
//Auth::routes();



//Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin','namespace'=>'Admin', 'middleware' => 'admin'], function()
{
    Route::get('dashboard',
        function (){
            return view('admin.dashboard.index');
        }
    );

    // users
    Route::resource('users', 'UsersController');

    // users
    //Route::post('users/user/{user_id}', 'UsersController@update')->name('updateUser');

    Route::get('users/cars/{user_id}','usersController@userCars')->name('userCars');
    // cars
    Route::resource('cars', 'CarsController');
    Route::post('cars/car/{id}', 'CarsController@update')->name('updateCar');



    Route::resource('main_categories', 'MainCategoriesController');
    Route::post('main_categories/category/{id}', 'MainCategoriesController@update')->name('updateMC');

    Route::resource('categories', 'CategoriesController');
    Route::post('categories/category/{id}', 'CategoriesController@update')->name('updateC');


    Route::resource('cities', 'CitiesController');
    Route::post('cities/city/{id}', 'CitiesController@update')->name('updateCity');




    // accounts
    Route::resource('accounts', 'AccountsController');
    Route::resource('contacts', 'ContactsController');
    Route::resource('pays', 'PaysController');


});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
