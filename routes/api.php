<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// register for user
Route::post('register', 'UserController@register');
//login user
Route::post('login', 'UserController@authenticate');
Route::get('', 'DataController@open');

/*
     * get Categories
     * */

Route::get('categories','Api\HomeController@getCategories');


/*
 * get main Categories
 * */

Route::get('main_categories','Api\HomeController@getMainCategories');


Route::group(['namespace' => 'Api'], function()
{

    // not need to login

});

Route::group(['middleware' => ['jwt.verify']], function() {

    Route::group(['prefix' => 'users'], function () {
        // get login user data
        Route::get('/', 'UserController@getAuthenticatedUser');
        // update user profile
        Route::post('update', 'UserController@update');
        //update password
        Route::post('update/password', 'UserController@updatePassword');
        // add played sport to profile

        // chats APIS
        Route::get('my-chat-with-users','UserController@my_chats');
        Route::get('my-chat-with-user/{user_id}','UserController@chatWithUser');
        Route::post('send/message/specific-user','UserController@sendMessageToUser');

        // cars APIS
        Route::post('/user/add_car','Api\CarsController@store');


        Route::get('/user/cars','UserController@userCars');

        // clicks mobile
        Route::post('user/click/mobile','UserController@mobileClick');

        // clicks whatsapp
        Route::post('user/click/whatsapp','UserController@whatsappClick');



        // get views to user

        Route::get('user/clicks/{user_id}','UserController@clicks');


        /*
         * upload the pay
         * */

        Route::post('user/pay','UserController@pay');

        Route::get('user/car/offline/{car_id}','Api\CarsController@offline');
        Route::get('user/car/online/{car_id}','Api\CarsController@online');

    });


    /*
     * Cars APIS
     * */
    Route::get('/users/cars','Api\CarsController@index');

    Route::get('/car/{car_id}/user','Api\CarsController@show');
    Route::post('/car/updatelocation','Api\CarsController@updateLocation');
    Route::get('/cars/main_categories/{main_category_id}','Api\CarsController@getCarsBasedOnMainCategory');


    /*
     * get Accounts
     * */
    Route::get('accounts','Api\HomeController@getAccounts');


    /*
     * get Settings
     * */
    Route::get('settings','Api\HomeController@getSetting');

    /*
     * get Cities
     * */

    Route::get('cities','Api\HomeController@getCities');




    /*
     * get contacts
     * */

    Route::post('contacts','Api\HomeController@contacts');
});
