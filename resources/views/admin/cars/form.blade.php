@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group col-md-12 pull-left">
    <label>اختار  المستخدم</label>
    {{
    Form::select('user_id',

    array(
        '' => 'اختار امستخدم ',
    ) + $users

        , (isset($car->user_id)? $car->user_id : null ), array('class' => 'form-control')

    )

    }}
</div>
<div class="form-group col-md-12 pull-left">
    <label>رقم العربة </label>
    {!! Form::text("car_number",(isset($car->car_number)? $car->car_number : null ),['class'=>'form-control ','placeholder'=>'اكتب رقم العربة هنا'])!!}
</div>


<div class="form-group col-md-12 pull-left">
    <label>اختار القسم الرئيسي </label>
    {{
    Form::select('main_category_id',

    array(
        '' => 'اختار القسم الرئيسي ',
    ) + $main_categories

        , (isset($car->main_category_id)? $car->main_category_id : null ), array('class' => 'form-control')

    )

    }}
</div>

<div class="form-group col-md-12 pull-left">
    <label>اختار القسم الفرعي </label>
    {{
    Form::select('category_id',

    array(
        '' => 'اختار القسم الفرعي ',
    ) + $categories

        , (isset($car->category_id)? $car->category_id : null ), array('class' => 'form-control')

    )

    }}
</div>

<div class="form-group col-md-12 pull-left">
    <label>صورة الهوية او الاقامة </label>
    @if(isset($car))
        <div class="old_car">
            <img class="img-responsive img-thumbnail" style="width: 200px; height: 120px" src="{{getImg($car->national_image)}}">
        </div>
    @endif
    {!! Form::file("national_image",null,['class'=>'form-control ','required'])!!}
</div>


<div class="form-group col-md-12 pull-left">
    <label>صورة الرخصة </label>
    @if(isset($car))
        <div class="old_car">
            <img class="img-responsive img-thumbnail" style="width: 200px; height: 120px" src="{{getImg($car->licence_image)}}">
        </div>
    @endif
    {!! Form::file("licence_image",null,['class'=>'form-control ','required'])!!}
</div>


<div class="form-group col-md-12 pull-left">
    <label>صورة الاستمارة </label>
    @if(isset($car))
        <div class="old_car">
            <img class="img-responsive img-thumbnail" style="width: 200px; height: 120px" src="{{getImg($car->st_image)}}">
        </div>
    @endif
    {!! Form::file("st_image",null,['class'=>'form-control ','required'])!!}
</div>


<div class="form-group col-md-12 pull-left">
    <label>صورة تفويض القيادة  </label>
    @if(isset($car))
        <div class="old_car">
            <img class="img-responsive img-thumbnail" style="width: 200px; height: 120px" src="{{getImg($car->copy_image)}}">
        </div>
    @endif
    {!! Form::file("copy_image",null,['class'=>'form-control ','required'])!!}
</div>


<div class="form-group col-md-12 pull-left">
    <label>صورة الامامية للعربة  </label>

    @if(isset($car))
        <div class="old_car">
            <img class="img-responsive img-thumbnail" style="width: 200px; height: 120px" src="{{getImg($car->front_image)}}">
        </div>
    @endif
    {!! Form::file("front_image",null,['class'=>'form-control ','required'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>صورة الخلفية للعربة  </label>
    @if(isset($car))
        <div class="old_car">
            <img class="img-responsive img-thumbnail" style="width: 200px; height: 120px" src="{{getImg($car->back_image)}}">
        </div>
    @endif
    {!! Form::file("back_image",null,['class'=>'form-control ','required'])!!}
</div>



<div class="form-group col-md-12 pull-left">
    <div id="map" style="width: 100%; height: 300px;"></div>

    <div class="clearfix">&nbsp;</div>
    <div class="m-t-small">
        <div class="col-sm-4">
            <label class="p-r-small control-label">خط الطول</label>
        </div>
        <div class="col-sm-6">
            {{ Form::text('lat', (isset($car)? $car->lat : null),['id'=>'us_restaurant-lat','class'=>'form-control']) }}
        </div>
        <div class="col-sm-4">
            <label class="p-r-small  control-label">خط العرض </label>
        </div>
        <div class="col-sm-6">
            {{ Form::text('long', (isset($car)? $car->long : null),['id'=>'us_restaurant-lon','class'=>'form-control']) }}
        </div>
    </div>
</div>


<br>
<br>
<div class="text-center col-md-12">
    <div class="text-right">
        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
    </div>
</div>

@push('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&callback=initialize"></script>
    <script src="{{asset('admin\admin\assets\js\plugins\pickers\location\location.js')}}"></script>
    <script>
        $('#map').locationpicker({
            location: {
                latitude: "{{isset($admin) ? $admin->lat : 26.356534671227795}}",
                longitude:"{{isset($admin) ? $admin->long : 43.98283325283205}}"
            },
            radius: 300,
            inputBinding: {
                latitudeInput: $('#us_restaurant-lat'),
                longitudeInput: $('#us_restaurant-lon'),
                locationNameInput: $('#us_restaurant-address')
            },
            enableAutocomplete: false,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                // Uncomment line below to show alert on each Location Changed event
                //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
            }
        });

        $('select[name="role"]').change(function () {
            if ($(this).val() == 'shop')
            {
                $('.shipping').hide();
                $("select[name='shipping_id']").removeAttr('required');
                $('.trades').show();
                $("select[name='trade_id']").prop('required','required');
            }else if  ($(this).val()  =='shipping')
            {
                $('.shipping').show();
                $("select[name='shipping_id']").prop('required','required');
                $('.trades').hide();
                $("select[name='trade_id']").removeAttr('required');

            }else{
                $('.trades').hide();
                $("select[name='trade_id']").removeAttr('required');
                $('.shipping').hide();
                $("select[name='shipping_id']").removeAttr('required');
            }
        });
    </script>
@endpush