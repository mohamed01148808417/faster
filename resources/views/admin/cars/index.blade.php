@extends('admin.layouts.home')
@section('title')
العربات
@endsection

@section('content')


@section('content')

    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">كل العربات</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>


        <div class="panel-body">
            <div class="selected">
                <div class="row">

                            {!!Form::open( [
                             'class'=>'form phone_validate', 'method' => 'get','files' => true]) !!}

                            <div class="text-center col-md-6">
                            <div class="text-left">
                            {{ Form::select('main_category_id',
                            array(
                                '' => 'اختار القسم الرئيسي ',
                            ) + $main_categories

                                , null, array('class' => 'form-control')

                            )

                            }}
                            </div>
                            </div>

                            <div class="text-center col-md-6">
                                <div class="text-right">
                                    <button type="submit" class="btn btn-success">ارسال<i class="icon-arrow-left13 position-right"></i></button>
                                </div>
                            </div>
                            {!!Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
          عرض كل العربات
        </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th> # </th>
                <th>رقم العربة</th>
                <th>المستخدم</th>
                <th>القسم الرئيسي </th>
                <th>القسم الفرعي </th>
                <th> صورة الهوية </th>
                <th>العمليات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($cars as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->car_number}}</td>
                    <td>{{$item->user->name}}</td>
                    <td>{{$item->main_category->title}}</td>
                    <td>{{$item->category->title}}</td>

                    <td><img style="width: 200px; height: 120px; border-radius: 0 !important;" src="{{getImg($item->national_image)}}" /> </td>

                    {!!Form::open( ['route' => ['cars.destroy',$item->id] ,
                    'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                    {!!Form::close() !!}
                    <td>
                        <a href="{{route('cars.edit',
                        ['id'=>$item->id])}}" data-toggle="tooltip"
                           data-original-title="تعديل">
                            <i class="icon-pencil7 text-inverse" style="margin-left: 10px"></i> </a>
                        <a href="#" onclick="Delete({{$item->id}})" data-toggle="tooltip" data-original-title="حذف">
                            <i class="icon-trash text-inverse text-danger" style="margin-left: 10px"></i> </a>
                    </td>

                </tr>
                <!-- Modal -->
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

    <div class="links">
        {{ $cars->links() }}
    </div>




    <script>
        function Delete(id) {
            var item_id=id;
            console.log(item_id);
            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذا المستخدم ؟",
                icon: "warning",
                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('delete-form'+item_id).submit();
                }
                else{
                    swal("تم االإلفاء", "حذف القسم تم الغاؤه",'info',{buttons:'موافق'});
                }
            });
        }



    </script>

    <div class="row">
        <div id="container">
            <canvas id="canvas"></canvas>
        </div>

    </div>

@endsection


