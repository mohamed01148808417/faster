@extends('admin.layouts.home')
@section('title','الاحصائيات')
@section('content')

    <div class="row">
        <div class="col-xs-4 col-sm-6 col-md-3 one-statistic">
            <div class="panel panel-body bg-warning has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini">عدد المستخدمين</span>
                        <h3 class="no-margin">{{count(App\User::all())}}</h3>

                    </div>

                    <div class="media-right media-middle">
                        <i class=" icon-users2 icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-4 col-sm-6 col-md-3 one-statistic">
            <div class="panel panel-body bg-info has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini">عدد الفروع</span>
                        <h3 class="no-margin">{{count(App\Category::all())}}</h3>

                    </div>

                    <div class="media-right media-middle">
                        <i class=" fas fa-building icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-4 col-sm-6 col-md-3 one-statistic">
            <div class="panel panel-body bg-success has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini">عدد الاقسام الرئيسيه</span>
                        <h3 class="no-margin">{{count(App\MainCategory::all())}}</h3>

                    </div>

                    <div class="media-right media-middle">
                        <i class="fas fa-code-branch icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-xs-4 col-sm-6 col-md-3 one-statistic">
            <div class="panel panel-body bg-warning has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini">عدد العربات</span>
                        <h3 class="no-margin">{{count(App\Car::all())}}</h3>

                    </div>

                    <div class="media-right media-middle">
                        <i class="fas fa-cubes icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-4 col-sm-6 col-md-3 one-statistic">
            <div class="panel panel-body bg-info has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini">عدد الحسابات</span>
                        <h3 class="no-margin">{{count(App\Account::all())}}</h3>

                    </div>

                    <div class="media-right media-middle">
                        <i class="fas fa-audio-description icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-4 col-sm-6 col-md-3 one-statistic">
            <div class="panel panel-body bg-success has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini">عدد الطلبات المكتملة</span>
                        <h3 class="no-margin">{{count(App\City::all())}}</h3>

                    </div>
                    <div class="media-right media-middle">
                        <i class="fas fa-cart-arrow-down icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-4 col-sm-6 col-md-3 one-statistic">
            <div class="panel panel-body bg-danger has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini">عدد وسائط الدفع</span>
                        <h3 class="no-margin">{{count(App\Pay::all())}}</h3>

                    </div>

                    <div class="media-right media-middle">
                        <i class="fas fa-image icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xs-4 col-sm-6 col-md-3 one-statistic">
            <div class="panel panel-body bg-indigo has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini">عدد الرسائل </span>
                        <h3 class="no-margin">{{App\Chat::count()}}</h3>

                    </div>

                    <div class="media-right media-middle">
                        <i class="icon-mail-read  icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>





    </div>

    <div class="row">
        <div id="container">
            <canvas id="canvas"></canvas>
        </div>
    </div>

@endsection
