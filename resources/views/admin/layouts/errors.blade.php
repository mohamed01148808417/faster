@if(Session::has('success'))
    <div class="alert  alert-styled-left alert-success" style="color: black">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">اغلاق</span></button>
        <span class="text-semibold"></span>عظيم !<a href="#" class="alert-link"> {{ Session::get('success') }} </a> .
    </div>
@endif

@if(Session::has('error'))

    <div class="alert bg-warning alert-styled-left">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">اغلاق</span></button>
        <span class="text-semibold"></span>عفواً !<a href="#" class="alert-link"> {{ Session::get('error') }} </a> .
    </div>
@endif