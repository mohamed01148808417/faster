
<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        {{--<a class="navbar-brand" href="/dashboard"><img src="/admin/assets/images/logo.png" alt=""></a>--}}
        <a class="navbar-brand" href="{{url('/admin/dashboard')}}">
            {{--        <img src="{{asset('admin/assets/images/logo.png')}}" class="position-left" alt="">  --}}
        </a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

        </ul>

        {{--<p class="navbar-text"><span class="label bg-success">متصل اﻵن</span></p>--}}

        <ul class="nav navbar-nav navbar-right">

            {{--<li class="dropdown language-switch">--}}
            {{--<a class="dropdown-toggle" data-toggle="dropdown">--}}
            {{--<img src="{{asset('admin/assets/images/flags/sa.png')}}" class="position-left" alt="">--}}
            {{--عربي--}}
            {{--</a>--}}
            {{--</li>--}}

            {{--      <li class=" dropdown notifications-wrapper">--}}
            {{--        <a href="#" data-toggle="dropdown" class="btn btn-link btn-float has-text">--}}
            {{--          <i class="icon-earth text-primary"></i>--}}
            {{--          <span class="noti-badge">{{auth()->user()->unreadNotifications()->whereType('App\Notifications\CartNotification')->count()}}</span>--}}
            {{--        </a>--}}
            {{--        <ul class="dropdown-menu animated fadeInUp the-notifications-wrapper">--}}
            {{--            @foreach(auth()->user()->unreadNotifications()->whereType('App\Notifications\CartNotification')->orderBy('id','desc')->get() as $notification)--}}
            {{--          <li>--}}
            {{--            تم اضافه عربه جديده للمشاهده--}}
            {{--            <a href="{{route('admin.cart-items.show',$notification->data['cart_id'])}}" class="makeAsRead" data-id="{{$notification->id}}">--}}
            {{--              اضغط هنا--}}
            {{--            </a>--}}
            {{--          </li>--}}
            {{--                @endforeach--}}
            {{--            <div class="fetchReaded">--}}

            {{--            </div>--}}
            {{--                <li class="read-all-notis">--}}
            {{--                    <a href="javascript:" class="readedNotifications">مشاهده كل الاشعارات</a>--}}
            {{--                </li>--}}
            {{--        </ul>--}}
            {{--      </li>--}}

            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <span>#</span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="{{route('logout')}}"><i class="icon-switch2"></i> تسجيل خروج</a></li>
                </ul>
            </li>




        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <!-- Main sidebar -->
        <div class="sidebar sidebar-main ">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left">
                                <img src="" class="img-circle img-sm" alt="">
                            </a>

                            <div class="media-body">
                                <span class="media-heading text-semibold">#</span>
                                <div class="text-size-mini text-muted">
                                    <i class="icon-pin text-size-small"></i> مدير الموقع
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /user menu -->

                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">


                        <ul class="navigation navigation-main navigation-accordion">


                            <!-- Main -->
                            <li class="navigation-header"><span>الاعدادات الرئيسية</span> <i class="icon-menu"
                                                                                             title="Main pages"></i></li>
                            <li class="{{(Request::is('dashboard') ? 'active' : '')}}"><a href="/admin/dashboard"><i
                                            class="icon-home4"></i> <span>الصفحة الرئيسية</span></a></li>


                            <li>
                                <a href="#"><i class="icon-users2"></i> <span>الاعضاء</span></a>
                                <ul>
                                    <li class="{{(Request::is('dashboard/users') ? 'active' : '')}}">
                                        <a
                                                href="{{route('users.index')}}"><i class="icon-list"></i> كل الاعضاء</a>
                                    </li>
                                    <li class="{{(Request::is('admin/users/create') ? 'active' : '')}}"><a
                                                href="{{route('users.create')}}"><i class="icon-add-to-list"></i> اضافة

                                            عضو جديد</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#"><i class="icon-add-to-list"></i> <span>الاقسام الرئيسية </span></a>
                                <ul>
                                    <li class="{{(Request::is('main_categories.index') ? 'active' : '')}}">
                                        <a
                                                href="{{route('main_categories.index')}}"><i class="icon-list"></i> كل الاقسام</a>
                                    </li>
                                    <li class="{{(Request::is('main_categories.create') ? 'active' : '')}}"><a
                                                href="{{route('main_categories.create')}}">
                                            <i class="icon-add-to-list"></i> اضافة قسم جديد</a>
                                    </li>
                                </ul>
                            </li>



                            <li>
                                <a href="#"><i class="icon-add"></i> <span>الاقسام الفرعية </span></a>
                                <ul>
                                    <li class="{{(Request::is('categories.index') ? 'active' : '')}}">
                                        <a
                                                href="{{route('categories.index')}}"><i class="icon-list"></i> كل الاقسام</a>
                                    </li>
                                    <li class="{{(Request::is('categories.create') ? 'active' : '')}}"><a
                                                href="{{route('categories.create')}}"><i class="icon-add-to-list"></i> اضافة قسم جديد</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#"><i class="icon-archive"></i> <span>المدن</span></a>
                                <ul>
                                    <li class="{{(Request::is('cities.index') ? 'active' : '')}}">
                                        <a
                                                href="{{route('cities.index')}}"><i class="icon-list"></i> كل المدن</a>
                                    </li>
                                    <li class="{{(Request::is('cities.create') ? 'active' : '')}}"><a
                                                href="{{route('cities.create')}}"><i class="icon-add-to-list"></i> اضافة مدينة جديدة</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#"><i class="icon-car2"></i> <span>العربات</span></a>
                                <ul>
                                    <li class="{{(Request::is('cars.index') ? 'active' : '')}}">
                                        <a
                                                href="{{route('cars.index')}}"><i class="icon-list"></i> كل العربات</a>
                                    </li>
                                    <li class="{{(Request::is('cars.create') ? 'active' : '')}}"><a
                                                href="{{route('cars.create')}}"><i class="icon-add-to-list"></i> اضافة عربه جديدة </a>
                                    </li>
                                </ul>
                            </li>


                            <li>
                                <a href="#"><i class="icon-car2"></i> <span>الحسابات</span></a>
                                <ul>
                                    <li class="{{(Request::is('accounts.index') ? 'active' : '')}}">
                                        <a
                                                href="{{route('accounts.index')}}">
                                            <i class="icon-list"></i> كل الحسابات</a>
                                    </li>
                                    <li class="{{(Request::is('accounts.create') ? 'active' : '')}}"><a
                                                href="{{route('accounts.create')}}">
                                            <i class="icon-add-to-list"></i> اضافة حساب جديد </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#"><i class="icon-car2"></i> <span>الدفع</span></a>
                                <ul>
                                    <li class="{{(Request::is('pays.index') ? 'active' : '')}}">
                                        <a
                                                href="{{route('pays.index')}}">
                                            <i class="icon-list"></i> كل عمليات الدفع</a>
                                    </li>

                                </ul>
                            </li>

                            <li>
                                <a href="#"><i class="icon-car2"></i> <span>الرسايل</span></a>
                                <ul>
                                    <li class="{{(Request::is('contacts.index') ? 'active' : '')}}">
                                        <a
                                                href="{{route('contacts.index')}}">
                                            <i class="icon-list"></i> كل الرسايل</a>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>
        <!-- /main sidebar -->
    <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><a href="{{Session::get('_previous')['url']}}" >
                                <i class="icon-arrow-right6 position-left"></i></a> <span class="text-semibold">لوحة تحكم فاستر </span> - @yield('title')</h4>
                    </div>

                    <div class="heading-elements">
                        <div class="heading-btn-group">
                            <a href="{{asset('dashboard')}}" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>بعض الاحصائيات</span></a>
                            <a href="#" class="btn btn-link btn-float has-text"><i class="icon-users text-primary"></i> <span>أعضاء الاداره</span></a>
                        </div>

                    </div>
                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="{{asset('dashboard')}}"><i class="icon-home2 position-left"></i> لوحة التحكم</a></li>
                        <li class="active">@yield('title')</li>
                    </ul>
                </div>
            </div>
            <!-- /page header -->        <!-- Content area -->
            <div class="content">
            @include('admin.layouts.errors')