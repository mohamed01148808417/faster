
<!-- Footer -->
<div class="footer text-muted text-center">
    &copy; 2018. <a href="#">تم التطوير والبرمجة</a> بواسطة <a href="#" target="_blank">شركة فكرة للحلول البرمجية </a>
</div>
<!-- /footer -->

</div>
<!-- /content area -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->
</div>
@stack('scripts')
</body>
</html>
