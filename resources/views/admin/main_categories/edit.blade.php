@extends('admin.layouts.home')
@section('title')
 اضافة الاقسام الرئيسية
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">تعديل قسم رئيسي  </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    {!!Form::open( ['route' => ['updateMC' , $category->id],
                    'class'=>'form', 'method' => 'Post','files' => true]) !!}

                        @include('admin.main_categories.form')

                    {!!Form::close() !!}
                </div>
            </div>
        </div>
    </div>
<!-- #END# Basic Validation -->
@endsection

@section('script')

    <script type="text/javascript" src="{{asset('admin/admin/assets/js/pages/form_layouts.js')}}"></script>
@endsection
