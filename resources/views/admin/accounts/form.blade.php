@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group col-md-12 pull-left">
    <label> الرقم المسلسل للحساب  </label>
    {!! Form::text("bank_id",(isset($account))?$account->bank_id :
    null,['class'=>'form-control ','placeholder'=>' الرقم المسلسل للحساب  '])!!}
</div>


<div class="form-group col-md-12 pull-left">
    <label>اسم الحساب </label>
    {!! Form::text("bank_name",(isset($account))?$account->bank_name :
     null,['class'=>'form-control ','placeholder'=>'اكتب اسم الحساب هنا'])!!}
</div>


<div class="form-group col-md-12 pull-left">
    <label> رقم ال IBAN </label>
    {!! Form::text("account_iban",(isset($account))?$account->account_IBAN :
    null,['class'=>'form-control ','placeholder'=>'اكتب رقم ال IBAN هنا'])!!}
</div>
<br>
<br>
<div class="text-center col-md-12">
    <div class="text-right">
        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
    </div>
</div>
