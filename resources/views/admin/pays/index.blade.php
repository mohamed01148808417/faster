@extends('admin.layouts.home')
@section('title')
عمليات الدفع
@endsection

@section('content')


@section('content')

    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">عمليات الدفع</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
كل عمليات الدفع         </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th> # </th>
                <th>الاسم </th>
                <th>صورة الدفع</th>
                <th>العمليات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($pays as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->user->name}}</td>
                    <td><img src="{{getImg($item->image)}}"/></td>
                    <td>
                        <a href="{{route('users.edit',
                        ['id'=>$item->user_id])}}" data-toggle="tooltip"
                           data-original-title="تعديل">
                            <i class="icon-pencil7 text-inverse" style="margin-left: 10px"></i> </a>

                        <a href="#" onclick="Delete({{$item->id}})" data-toggle="tooltip" data-original-title="حذف">
                            <i class="icon-trash text-inverse text-danger" style="margin-left: 10px"></i> </a>
                        {!!Form::open( ['route' => ['pays.destroy',$item->id] ,
                        'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                        {!!Form::close() !!}
                    </td>

                </tr>
                <!-- Modal -->
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

    <div class="links">
        {{ $pays->links() }}
    </div>

    <script>
        function Delete(id) {
            var item_id=id;
            console.log(item_id);
            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف  ؟",
                icon: "warning",
                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('delete-form'+item_id).submit();
                }
                else{
                    swal("تم االإلفاء", "حذف القسم تم الغاؤه",'info',{buttons:'موافق'});
                }
            });
        }



    </script>

    <div class="row">
        <div id="container">
            <canvas id="canvas"></canvas>
        </div>

    </div>
@endsection


