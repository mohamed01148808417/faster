@extends('admin.layouts.home')
@section('title')
المستخدمين
@endsection

@section('content')


@section('content')

    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">كل العضويات</h5>
            <div class="heading-elements">
                <ul class="icons-list">

                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            {!!Form::open( ['route' => null,
                            'class'=>'form phone_validate', 'method' => 'get','files' => true]) !!}

            <div class="text-center col-md-6">
                <div class="text-left">
                    {{ Form::select('user_type',
                    array(
                        '' => 'اختار نوع المستخدم ',
                        '1' => 'المستخدمين',
                        '2' => 'مقدمي الخدمة',
                    )
                        , null, array('class' => 'form-control')
                    )

                    }}
                </div>
            </div>

            <div class="text-center col-md-6">
                <div class="text-right">
                    <button type="submit" class="btn btn-success">ارسال<i class="icon-arrow-left13 position-right"></i></button>
                </div>
            </div>
            {!!Form::close() !!}

            <br>
            <br>
            عرض كل العضويات والتحكم بهم وبكل العمليات الخاصة بهم مع امكانية البحث وتصدير تقارير وملفات وطباعتهم
        </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th> # </th>
                <th>الاسم </th>
                <th>البريد</th>
                <th>الموبايل</th>
                <th>المدينة</th>
                <th>موبايل</th>
                <th> واتس اب</th>
                <th>العمليات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->phone}}</td>
                    <td>{{($item->city_id)? $item->city->title : 'no city now'}}</td>
                    @if($item->user_type == 2)
                        <td>{{$item->clicks->first()->mobile_click }}</td>
                    @else
                        <td>null</td>
                        @endif
                    @if($item->user_type == 2)
                        <td>{{$item->clicks->first()->whatsapp_click }}</td>
                    @else
                        <td>null</td>

                    @endif
                    {!!Form::open( ['route' => ['users.destroy',$item->id] ,
                    'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                    {!!Form::close() !!}
                    <td>
                        <a href="{{route('users.edit',
                        ['id'=>$item->id])}}" data-toggle="tooltip"
                           data-original-title="تعديل">
                            <i class="icon-pencil7 text-inverse" style="margin-left: 10px"></i> </a>

                       @if($item->user_type == 2)
                            <a href="{{route('userCars',
                        ['id'=>$item->id])}}" data-toggle="tooltip"
                               data-original-title="عربات المستخدم">
                                <i class="icon-car text-inverse" style="margin-left: 10px"></i> </a>
                       @endif

                        <a href="#" onclick="Delete({{$item->id}})" data-toggle="tooltip" data-original-title="حذف">
                            <i class="icon-trash text-inverse text-danger" style="margin-left: 10px"></i> </a>
                    </td>

                </tr>
                <!-- Modal -->
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

    <div class="links">
        {{ $users->links() }}
    </div>




    <script>
        function Delete(id) {
            var item_id=id;
            console.log(item_id);
            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذا المستخدم ؟",
                icon: "warning",
                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('delete-form'+item_id).submit();
                }
                else{
                    swal("تم االإلفاء", "حذف القسم تم الغاؤه",'info',{buttons:'موافق'});
                }
            });
        }



    </script>

    <div class="row">
        <div id="container">
            <canvas id="canvas"></canvas>
        </div>

    </div>

@endsection


