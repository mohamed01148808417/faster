@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group col-md-12 pull-left">
    <label>اختار نوع المستخدم الرئيسي </label>
    {{
    Form::select('user_type',

    array(
        '' => 'اختار النوع ',
        '1' => 'مستخدم',
        '2' => 'مقدم خدمة / سائق',
    )

        , null, array('class' => 'form-control','id'=>'user_type')

    )

    }}
</div>


<div class="form-group col-md-12 pull-left main_category">
    <label>اختار القسم الرئيسي </label>
    {{
    Form::select('main_category',

    array(
        '' => 'اختار النوع ',
    ) + $categories

        , null, array('class' => 'form-control main_category','id'=>'main_category')

    )

    }}
</div>

<div class="form-group col-md-12 pull-left">
    <label>الاسم </label>
    {!! Form::text("name",null,['class'=>'form-control ','placeholder'=>'اكتب الاسم هنا'])!!}
</div>

<div class="form-group col-md-12 pull-left points">
    <label>النقاط </label>
    {!! Form::text("points",null,['class'=>'form-control points','id'=>'points','placeholder'=>'عدد النقاط '])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>البريد الالكتورني </label>
    {!! Form::text("email",null,['class'=>'form-control ','placeholder'=>'اكتب البريد الالكتروني هنا'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>رقم الجوال </label>
    {!! Form::text("phone",null,['class'=>'form-control ','placeholder'=>'اكتب رقم الجوال هنا'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>الرقم السري </label>
    {!! Form::input("password",'password',null,['class'=>'form-control ','placeholder'=>'اكتب الرقم السري هنا'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>اعد كتابه الرقم السري </label>
    {!! Form::input("password",'password_confirmation',null,['class'=>'form-control ',
    'placeholder'=>'اعد كتابه الرقم السري هنا'])!!}
</div>


<div class="form-group col-md-12 pull-left">
    <label>اختار المدينة</label>
    {{
    Form::select('city',

    array(
        '' => 'اختار النوع ',
    ) + $cities

        , null, array('class' => 'form-control')

    )

    }}
</div>



<div class="form-group col-md-12 pull-left">
    <label>صورة المستخدم </label>
    {!! Form::file("image",null,['class'=>'form-control ','required'])!!}
</div>


<div class="form-group col-md-12 pull-left">
    <div id="map" style="width: 100%; height: 300px;"></div>

    <div class="clearfix">&nbsp;</div>
    <div class="m-t-small">
        <div class="col-sm-4">
            <label class="p-r-small control-label">خط الطول</label>
        </div>
        <div class="col-sm-6">
            {{ Form::text('lat', null,['id'=>'us_restaurant-lat','class'=>'form-control']) }}
        </div>
        <div class="col-sm-4">
            <label class="p-r-small  control-label">خط العرض </label>
        </div>
        <div class="col-sm-6">
            {{ Form::text('long', null,['id'=>'us_restaurant-lon','class'=>'form-control']) }}
        </div>
    </div>
</div>


<br>
<br>
<div class="text-center col-md-12">
    <div class="text-right">
        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
    </div>
</div>

<style>

</style>
@push('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&callback=initialize"></script>
    <script src="{{asset('admin\admin\assets\js\plugins\pickers\location\location.js')}}"></script>
    <script>
        $('#map').locationpicker({
            location: {
                latitude: "{{isset($admin) ? $admin->lat : 26.356534671227795}}",
                longitude:"{{isset($admin) ? $admin->long : 43.98283325283205}}"
            },
            radius: 300,
            inputBinding: {
                latitudeInput: $('#us_restaurant-lat'),
                longitudeInput: $('#us_restaurant-lon'),
                locationNameInput: $('#us_restaurant-address')
            },
            enableAutocomplete: false,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                // Uncomment line below to show alert on each Location Changed event
                //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
            }
        });


        var user_type = $('#user_type');



        user_type.change(function () {
            if(user_type.val() == 1){

                $('#main_category').parent().css('display','none');
                $('#points').parent().css('display','none');
            }else if (user_type.val() == 2){
                // it is service provider
                $('#main_category').parent().css('display','block');
                $('#points').parent().css('display','block');
            }
        });
    </script>
@endpush